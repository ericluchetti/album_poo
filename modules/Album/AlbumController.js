import AlbumModel from "./AlbumModel.js";

export default class AlbumController {

    albumModel;

    constructor() {
    }

    create() {
        try {
            let albumModel = new AlbumModel();
            let imagem = albumModel.create();

            return imagem;
        } catch (error) {
            throw error;
        }
    }

    deleted(index) {
        try {
            let albumModel = new AlbumModel();

            albumModel.deleted(index);

        } catch (error) {
            console.log(error);

            throw error;
        }
    }

    getImagens() {
        try {
            let albumModel = new AlbumModel();
            let imagens = albumModel.getImagens();

            return imagens;
        } catch (error) {
            throw error;
        }
    }

}

const albumController = new AlbumController();

document.getElementById("create").addEventListener('click', albumController.create);