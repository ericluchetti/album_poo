import AlbumController from "./AlbumController.js";

export default class AlbumView {



    constructor() {

    }

    read() {
        try {
            let albumController = new AlbumController();
            let albumImagens = albumController.getImagens();
            let retorno = ``;
            let index = 0;

            for (let item of albumImagens) {
                retorno += `               
                    <img src="${item.imagem}" class="small-img" data-bs-toggle="modal" data-bs-target="#modal${index}" id="imagem${index}">
                `;

                index++;
            }

            albumView.readModal();

            document.getElementById('album').innerHTML = retorno;

        } catch (error) {
            console.log(error);
        }
    }

    deleted(index) {
        try {
            let albumController = new AlbumController();

            let confirmacao = confirm(`Tem certeza de que deseja excluir?`);
            if (confirmacao == true) {
                albumController.deleted(index);

                albumView.read();
            }
        } catch (error) {
            console.log(error);

            throw error;
        }

    }

    readModal() {
        try {
            let albumController = new AlbumController();
            let albumImagens = albumController.getImagens();
            let retornoModal = ``
            let index = 0;

            for (let item of albumImagens) {

                retornoModal += `
                <div class="modal fade" id="modal${index}" tabindex="-1" data-bs-backdrop="static" aria-labelledby="modalLabel"
                aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content" style="background-color: rgba(255, 255, 255, 0.7);">
                            <div class="modal-header">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="text-align: right;"></button>
                            </div>
                            <div class="modal-body">
                                <img src="${item.imagem}" style="max-width: 1100px">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal"
                                    id="excluirImagem" onclick="deleted(${index})">Excluir</button>
                            </div>
                        </div>
                    </div>
                </div>
                `

                index++;
            }

            document.getElementById('modal').innerHTML = retornoModal;

        } catch (error) {
            console.log(error);
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------//

    previewFile(event) {
        try {
            let reader = new FileReader();
            let file = event.target.files[0];

            reader.readAsDataURL(file);
            reader.onloadend = () => {
                preview.src = reader.result;
                document.getElementsByTagName("textarea")[0].value = reader.result;
            }
        } catch (error) {
            alert(error.message)
        }
    }

    convertBase64(event) {
        try {
            let file = {}
            file.valor = event.target.value.replace(/^data:image\/[a-z]+;base64,/, "");
            preview.src = `data:image/png;base64,${file}`;

            return file;
        } catch (error) {
            alert(error.message);
        }
    }

    previewImage() {
        try {
            let a = document.createElement("a");
            a.href = preview.src;

        } catch (error) {
            alert(error.message);
        }
    }

}

const albumView = new AlbumView();

let albumController = new AlbumController();

let preview = document.querySelector('img');

document.getElementsByTagName("input")[0].addEventListener('change', albumView.previewFile);
document.getElementsByTagName("input")[0].addEventListener('change', albumView.previewImage);
document.getElementsByTagName("textarea")[0].addEventListener('input', albumView.convertBase64);
document.getElementById("create").addEventListener('click', albumView.read);
document.getElementById("deletarAlbum").addEventListener('click', albumView.read);

window.onload = function () {
    albumView.read();
}